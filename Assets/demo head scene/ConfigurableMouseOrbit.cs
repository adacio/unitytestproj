using UnityEngine;
using System.Collections;

namespace JBrothers.PreIntegratedSkinShader {
	[AddComponentMenu("Camera-Control/Configurable Mouse Orbit")]
	public class ConfigurableMouseOrbit : MonoBehaviour {
		public Transform target;
		public float distance = 3.0f;
		public float zoomSpeed = 1.0f;
		public float distanceMin = 0.2f;
		public float distanceMax = 10f;

		public float xSpeed = 250.0f;
		public float ySpeed = 120.0f;

		public float yMinLimit = 0f;
		public float yMaxLimit = 90f;
		
		private float x = 0.0f;
		private float y = 0.0f;
		
		public bool centerToAABB = true;
		public bool ScrollZoom = true;
		
		public MouseButton mouseButton = MouseButton.Left;
		
		public bool lockCursor = true;
		
		public enum MouseButton : int {
			None = -1,
			Left = 0,
			Middle = 2,
			Right = 1
		}
		
		void Start () {
		    if (!target)
				return;
			
		    var angles = transform.eulerAngles;
		    x = angles.y;
		    y = angles.x;
		
			// Make the rigid body not change rotation
		   	if (GetComponent<Rigidbody>())
				GetComponent<Rigidbody>().freezeRotation = true;
		}
		
		private bool CursorLocked {
			#if UNITY_5_0 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5
			get {
				return Cursor.lockState == CursorLockMode.Locked;
			}
			set {
				Cursor.lockState = value ? CursorLockMode.Locked : CursorLockMode.None;
			}
			#else // Unity 3/4
			get {
				return Screen.lockCursor;
			}
			set {
				Screen.lockCursor = value;
			}
			#endif
		}

		void Update() {
		    if (!target)
				return;
			
			if (mouseButton == MouseButton.None || Input.GetMouseButton((int)mouseButton)) {
				if (!CursorLocked && lockCursor)
					CursorLocked = true;
		        x += Input.GetAxis("Mouse X") * xSpeed * 0.02f;
		        y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;
			} else {
				if (CursorLocked && lockCursor)
					CursorLocked = false;
			}
			
			if (ScrollZoom) {
				float wheelDelta = Input.GetAxis("Mouse ScrollWheel");
				if (wheelDelta != 0f)
					distance = Mathf.Clamp(distance * (1.0f - wheelDelta * zoomSpeed), distanceMin, distanceMax);
			}
	 		y = ClampAngle(y, yMinLimit, yMaxLimit);
			
	        Quaternion rotation = Quaternion.Euler(y, x, 0f);
	        Vector3 position = rotation * new Vector3(0.0f, 0.0f, -distance) + target.position;
			if (centerToAABB && target.GetComponent<Renderer>())
				position += target.transform.InverseTransformPoint(target.GetComponent<Renderer>().bounds.center);
	        
	        transform.localRotation = rotation;
	        transform.localPosition = position;
		}

		static float ClampAngle(float angle, float min, float max) {
			if (angle < -360f)
				angle += 360f;
			if (angle > 360f)
				angle -= 360f;
			return Mathf.Clamp(angle, min, max);
		}

	}
}